tc=int(raw_input())
while tc:
	first = raw_input()
	data=first.split(" ")
	n=int(data[0])
	r=int(data[1])
	maxVal=0
	rValues=[]
	while r:
		l=raw_input()
		rValues.append(l.split(" "))
		r = r-1
	for x in range(len(rValues)):
		f=rValues[x]
		w1,x1,y1 = [int(e) for e in f]
		for y in range(x):
			s=rValues[y]
			w2,x2,y2 = [int(e) for e in s]
			curVal = w1-max(abs(x1-x2),abs(x2-y2))
			newVal = curVal + w2
			if maxVal < newVal:
				maxVal = newVal
	print maxVal
	tc = tc -1
