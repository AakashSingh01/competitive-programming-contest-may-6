import random
num = 2
filein = "test_cases/{num}/in.txt".format(num=num)
fileout = "test_cases/{num}/out.txt".format(num=num)
fin = open(filein, "w")
fout = open(fileout, "w")

def get_matrix(N):
	return [[ 0 for i in xrange(N) ] for i in xrange(N) ]
	

def update_cells(matrix, M, x1, y1, x2, y2):
	x11 = x1
	y11 = y1+1
	x22 = x2
	y22 = y2-1
	# print x11, y11, x22, y22
	# print "!@@!@!"
	# print "()()()()()()"

	if x11 > -1:
		# print "j", x11, x2, x11, M
		while x11 < x2 and x11 < len(matrix) and y1 > -1:
			# print "jk"
			matrix[x11][y1] += M
			x11 += 1
	
	if y11 > -1:
		while y11 < y2 and y11 < len(matrix[0]) and x1 > -1:
			matrix[x1][y11] += M
			y11 += 1			
	
	if x22 < len(matrix):
		while x22 >= x1 and x22 >= 0 and y2 < len(matrix[0]):
			matrix[x22][y2] += M
			x22 -= 1

	if y22 <= len(matrix[0]):
		while y22 >= y1 and y22 >= 0 and x2 > -1 and x2 < len(matrix) and y22 < len(matrix[0]):
			print x2, y22
			matrix[x2][y22] += M
			y22 -= 1
	print "updated"

def update_neighbours(matrix, M, x, y):
	m = 1
	# for line in matrix:
	# 	print line
	while m < M:		
		update_cells(matrix, M-m, x-m, y-m, x+m, y+m)
		m += 1

test_cases = 10
fin.write(str(test_cases)+"\n")
for counter in xrange(test_cases):
	N = random.randint(4, 100)
	R = random.randint(1, 100)
	fin.write("TESTCASE\n")
	fout.write("TESTCASE\n")
	s = "{N} {R}\n".format(N=N, R=R)
	# print s, "-"
	# print "TESTCASE", counter
	fin.write(s)
	ans = 0
	for i in xrange(R):
		M = random.randint(1, N)
		x = random.randint(0, N-1)
		y = random.randint(0, N-1)
		s = "{M} {x} {y}\n".format(M=M, x=x, y=y)
		fin.write(s)
		print N, R, s, "-"
		matrix = get_matrix(N)
		for counter1 in xrange(R):
			# M, x, y = map(int, raw_input().split(' '))
			matrix[x][y] += M
			update_neighbours(matrix, M, x, y)
		for line in matrix:
			mx = max(line)
			if ans < mx:
				ans = mx
		# for line in matrix:
		# 	print line
		# print make_null_session()
		fout.write(""+str(ans)+"\n")

