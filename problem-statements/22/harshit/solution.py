import unittest



def check_parity(ip_string):
	ones_count = ip_string[1:].count('1')
	if ones_count % 2:
		if ip_string[0] == '0':
			return "Y"
		else:
			return "N"
	elif ones_count % 2 == 0:
		if ip_string[0] == '1':
			return "Y"
		else:
			return "N"

# print check_parity('11111111')
# print number
# print check_parity('01100100')

class SimpleTest(unittest.TestCase):

	def test_all_ones(self):
		ip_str = '11111111'
		self.assertEqual(check_parity(ip_str), 'N')

	def test_all_zeros(self):
		ip_str = '0'*8
		self.assertEqual(check_parity(ip_str), 'N')

	def test_01100100(self):
		ip_str = '01100100'
		self.assertEqual(check_parity(ip_str), 'Y')

	def test_alterantes_with_0(self):
		ip_str = '01'*4
		self.assertEqual(check_parity(ip_str), 'N')

	def test_alterantes_with_1(self):
		ip_str = '10'*4
		self.assertEqual(check_parity(ip_str), 'N')		

 


# fout.write(check_parity(str(bin(number)[2:]))		

if __name__ == "__main__":
    unittest.main()