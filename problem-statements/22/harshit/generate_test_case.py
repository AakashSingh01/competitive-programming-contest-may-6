from solution import check_parity

DEBUG = False

numbers = [ i for i in xrange(128) ]
fin = open("test_cases/1/in.txt", "w")
fout = open("test_cases/1/out.txt", "w")

for number in numbers:
	number = '{num}'.format(num=str(bin(number)[2:]))
	number = number.zfill(7)
	if DEBUG:
		print number
		print check_parity('0'+number)
		print check_parity('1'+number)
	fin.write('0'+number+'\n')
	fin.write('1'+number+'\n')
	fout.write(check_parity('0'+number)+'\n')
	fout.write(check_parity('1'+number)+'\n')