### Problem Statement 7

Cryptography is one of the favorite domains of Shubham.
He likes to encode stuff in many different ways.
This time he thought of using strings to encode numbers.
So he took a number, say for example 12345, and encoded it as the string ABCDE where each letter in the string is associated with its numerical position in the English alphabet. (1 = A, 2 = B, and so on �)
But there was one slight issue with this encoding. More than one string can be formed!
For example 12345 can be encoded as LCDE (12 = L, 3 = C, 4 = D and 5 = E)

Then he got curious! He wanted to know how many such strings are possible given a particular number. Can you help him do this?

**Input Format:**

The first line consists of the number of testcases T.

Each testcase consists of a single integer X, the number to be encoded.


**Output Format:**

For each testcase print the desired output in a new line.


**Constraints:**

1 <= T <= 100

1 <= X <= 10<sup>6</sup>

**Sample Input:**
```
1
1234
```

**Sample Output:**
```
3
```
